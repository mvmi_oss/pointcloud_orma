
# import libaries
import open3d as o3d
import numpy as np


#Opening the file and loading it into a numpy matrix
def open_pointcloud(filenev):
    pc=np.loadtxt(filenev,delimiter=' ')
    return pc

#Open3d visualizer function for displaying the point cloud into a Open3d canvas
def visualizer(pointcloudpath):
    if pointcloudpath.endswith(".xyz"):
        
        f=open(pointcloudpath)
        pc=open_pointcloud(f)
        pcd = o3d.geometry.PointCloud(o3d.utility.Vector3dVector(pc[:,:3]))

        #Color assignment
        original_colour = [0.9, 0.9, 0.9]  # The point clouds original colors are grey
        found_colour = [0,0,0] 
        other_colour = [0.6,0.6,0.6] #??? erre még valami jó nevet kéne találni
        calculated_labels=pc[:,3]
        colours=np.zeros((len(calculated_labels),3))
        colours[calculated_labels == 0]=original_colour
        colours[calculated_labels == 0]=original_colour
        colours[calculated_labels == 1]=found_colour
        colours[calculated_labels == 2]=other_colour
        pcd.colors =  o3d.utility.Vector3dVector(colours) #assining the colours to the geometry

        #Creating Open 3d visualizer
        vis = o3d.visualization.Visualizer()
        vis.create_window(visible=True)
        vis.add_geometry(pcd)

        vis.update_geometry(pcd)

        vis.poll_events()
        vis.update_renderer()

        o3d.visualization.draw_geometries([pcd])

        """# Capture the screen image
        your_png_path = os.path.join(output_dir, f"{os.path.splitext(filename)[0]}.png")
        vis.capture_screen_image(your_png_path)""" # If you wish to screen capture use this code snippet 
        f.close()




