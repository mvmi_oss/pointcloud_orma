#Gale 0.1.3
# Generating Variable outlier filter methods for Point clouds
"""

@author: Kiss Bence
"""

# import libaries
import numpy as np
import open3d as o3d
import os
import random
import subprocess
import sys
import tkinter as tk
import xml.etree.ElementTree as ET
from xml.dom.minidom import parse, parseString, Node

from PIL import Image, ImageTk
from tkinter import filedialog
from tkinter import messagebox

from Outlier_filtering import run_filtering,run_filteringxml, abod_method,cblof_method, copod_method,ecod_method, inne_method, knn_method, loda_method, lof_method, pca_method, rod_method, sampling_method # python script for the generating the ... (itt mit is generálunk le mint eredmény? a szűrt értékeket)
from Open3dvis import visualizer

class outliergeneration:
    def __init__(self, master):

        self.master = master
        master.title("GALE") #Display name of the Window

        # Object command fuctions

        def browse_directory(): #button_browse command function for setting the point cloud path
        
            filexyz =filedialog.askopenfile(initialdir= os.path.dirname(os.path.abspath(__file__)), mode='r', filetypes=[('XYZ Files', '*.xyz')]) # noqa: E501
            filepathxyz = filexyz.name
            pagename= self.entry_path.get()
            if pagename == "":
                self.entry_path.insert(0,filepathxyz)
            else:
                self.entry_path.delete(0, 'end')
                self.entry_path.insert(0,filepathxyz)

        def disable_toggle(): # xml_chkbutton command function for disabling the manual input buttons
            if state_xml_chknbttn.get():
                self.button_browse.config(state=tk.DISABLED)
                self.entry_path.config(state=tk.DISABLED)
                self.segmentation_option.config(state=tk.DISABLED)
                self.outlier_option.config(state=tk.DISABLED)
            else:
                self.button_browse.config(state=tk.NORMAL)
                self.entry_path.config(state=tk.NORMAL)
                self.segmentation_option.config(state=tk.NORMAL)
                self.outlier_option.config(state=tk.NORMAL)
        
        def edit_xml(filepath,xml_file_name): #button_xmledit command fuction for opening the xml file for editing in notepad         
            subprocess.run(['notepad',f'{os.path.join(filepath,xml_file_name)}'])

        def gen_output(filepath,xml_file_name):
            if state_xml_chknbttn.get():

                method_mapping = {
                    'abod_method': abod_method,
                    'cblof_method': cblof_method,
                    'copod_method': copod_method,
                    'ecod_method': ecod_method,
                    'inne_method': inne_method,
                    'knn_method': knn_method,
                    'loda_method': loda_method,
                    'lof_method': lof_method,
                    'pca_method': pca_method,
                    'rod_method': rod_method,
                    'sampling_method': sampling_method
                }

                # Parse the XML file
                
                tree = ET.parse(os.path.join(filepath,xml_file_name))
                root = tree.getroot()

                # Extract <def_odlist> into a list variable
                def_odlist = [method_mapping[method.text] for method in root.find('def_odlist')]

                # Extract <def_odnames> into a list of strings
                def_odnames = [name.text for name in root.find('def_odnames')]

                # Randomly choose one segmentation method from <segmentation_list>
                segmentation_elements = root.find('segmentation_list')
                segmentation_list = [segment.text for segment in segmentation_elements if segment.tag == 'segment']
                segmentation = random.choice(segmentation_list)

                # Extract segmentsize and bits
                segmentsize = int(segmentation_elements.find('segmentsize').text.strip())
                bits = int(segmentation_elements.find('bits').text.strip())

                # Extract <parameters> into a list of dictionaries
                parameters = {}
                for param in root.find('parameters'):
                    method_name = param.tag
                    param_dict = {child.tag: float(child.text) if '.' in child.text else int(child.text) for child in param}
                    parameters[method_name] = param_dict
                
                paths_element = root.find('paths')
                pc_path = paths_element.find('pc_path').text.strip()
                pc_name = paths_element.find('pc_name').text.strip()
                pointcloudpath = os.path.join(pc_path, pc_name)
                print(pointcloudpath) 

                run_filteringxml(pointcloudpath,def_odnames,def_odlist,segmentsize,bits,segmentation,parameters)
            else:
                segmentation= dropdownoptions_segmentation.get()
                outliermethod= dropdownoptions_outlier_alg.get()
                pointcloudpath= self.entry_path.get()
                messagebox.showinfo("Values", f"Segmentation: {segmentation}\nOutlier Method: {outliermethod}\nPoint cloud path: {pointcloudpath}")
                run_filtering(segmentation,outliermethod,pointcloudpath)
                
            if state_open3d_chkbttn.get():
                visualizer(pointcloudpath)         
            else:
                print('nincs vizualizáció')

        # Setting up the GUI layout

        #Pointcloud choosing
        self.lable=tk.Label(text="Point Cloud Path:")
        self.lable.place(x=20,y=20)   

        self.button_browse = tk.Button( text="Browse", command=browse_directory)
        self.button_browse.place(x=200,y=40)

        self.entry_path=tk.Entry(width=27)
        self.entry_path.place(x=23,y=45)

        #Chekbutton for using the open3d visualizer to show the outlier points
        self.vis_chkbutton=tk.Checkbutton(text="Visualiziation with Open3d",variable=state_open3d_chkbttn )
        self.vis_chkbutton.place(x=270,y=40)

        #Chekbutton for using XML instead of manual selection
        self.xml_chkbutton=tk.Checkbutton(text="Use XML input instead of manual",variable=state_xml_chknbttn,command=disable_toggle)
        self.xml_chkbutton.place(x=270, y=80)

        #Edit xml button
        self.button_xmledit=tk.Button(text="Edit XML",command=lambda:edit_xml(filepath,xml_file_name))
        self.button_xmledit.place(x=340, y=120)

        #Dropdown menus for choosing segmentation and outlier algorithms
        self.segmentation_option=tk.OptionMenu(root, dropdownoptions_segmentation, *segmentation_list)
        self.segmentation_option.place(x=20,y=90)
        self.downlable_seg=tk.Label(self.segmentation_option, bg="WHITE", width=16, height=16, image=dropdownImage)
        self.downlable_seg.place(relx=0.89, rely=0.06)

        self.outlier_option=tk.OptionMenu(root,dropdownoptions_outlier_alg,*outlier_list)
        self.outlier_option.place(x=20, y=135)
        self.downlable_alg=tk.Label(self.outlier_option, bg="WHITE", width=16, height=16, image=dropdownImage)
        self.downlable_alg.place(relx=0.89, rely=0.06)

        #Generate button
        self.button_generate=tk.Button(root,text='Generate',command=lambda:gen_output(filepath,xml_file_name))
        self.button_generate.place(x=93, y=200)

        #--------CONFIG----------
        self.segmentation_option.config(
            bg="WHITE",
            fg="BLACK",
            indicatoron=0,
            height=0,
            width=32
        )
        self.outlier_option.config(
            bg="WHITE",
            fg="BLACK",
            indicatoron=0,
            height=0,
            width=32
        )
        #--------CONFIG END----------

        #-------ACTIVE EVENTS--------
        #Visualizaztion section
        #def state_open3d_rdnbttn
        #Segmentation section
        def on_enter(event):
            self.downlable_seg.config(bg='#f0f0f0')
        def on_leave(event):
            self.downlable_seg.config(bg='WHITE')
        def on_lableclick(event):
            self.segmentation_option.event_generate('<Button-1>')
        self.segmentation_option.bind('<Enter>', on_enter)
        self.segmentation_option.bind('<Leave>', on_leave)
        self.downlable_seg.bind('<Button-1>', on_lableclick)

        #Algoritm section
        def on_enter(event):
            self.downlable_alg.config(bg='#f0f0f0')
        def on_leave(event):
            self.downlable_alg.config(bg='WHITE')
        def on_lableclick(event):
            self.downlable_alg.event_generate('<Button-1>')
        self.outlier_option.bind('<Enter>', on_enter)
        self.outlier_option.bind('<Leave>', on_leave)
        self.downlable_alg.bind('<Button-1>', on_lableclick)

        

        #-------ACTIVE EVENTS END--------


       




#-------MAIN-------
root = tk.Tk()
#ablak mérete
root.geometry('600x400')
root.minsize([600],[400])


#----------IMAGES-----------

#Main program path:
filepath=os.path.dirname(os.path.abspath(__file__))
#Names of the files:
xml_file_name="config.xml"
fileicon= 'outlier_icon_vector.png' #Icon
filedropdown='dropdown2.png' #Dropdown option menu symbol

#Maind window icon
icon_image = Image.open(os.path.join(filepath,fileicon) ) # image path
icon_icon = ImageTk.PhotoImage(icon_image) #transform it into an icon.
root.wm_iconphoto(False, icon_icon)

#Dropdown icon for tk.OptionMenu object
drop= Image.open(os.path.join(filepath,filedropdown)).resize([13,13]) #image path
dropdownImage=ImageTk.PhotoImage(drop)
#--------IMAGES END---------

#------DEFAULT VALUES-------

# Setting up the starting values of differnt tk objects
state_open3d_chkbttn = tk.BooleanVar()
state_xml_chknbttn=tk.BooleanVar()
dropdownoptions_segmentation=tk.StringVar(value='octree')
dropdownoptions_outlier_alg=tk.StringVar(value='ABOD')

#Content of the dropdown list:
segmentation_list=['octree','segment','step']
outlier_list=['ABOD','CBLOF','COPOD','ECOD','INNE','KNN','LODA','LOF','PCA','ROD','Sampling']
#------DEFAULT VALUES END-------




#Initializing the App
app = outliergeneration(root)
root.mainloop()