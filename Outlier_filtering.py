#Outlier filtering script

#import libaries
import numpy as np
import random
import time
import os

from pyod.models.abod import ABOD
from pyod.models.cblof import CBLOF
from pyod.models.copod import COPOD
from pyod.models.ecod import ECOD
from pyod.models.inne import INNE
from pyod.models.knn import KNN
from pyod.models.loda import LODA
from pyod.models.lof import LOF
from pyod.models.pca import PCA
from pyod.models.rod import ROD
from pyod.models.feature_bagging import FeatureBagging

def run_filtering(segmentation,outliermethod,pointcloudpath):
    
    pcnames=pointcloudpath
    def_odnames=outliermethod
    def_odlist=[abod_method,cblof_method,copod_method,ecod_method,inne_method,
                knn_method,loda_method,lof_method,pca_method,rod_method]
    akt_odlist=[method for method in def_odlist if def_odnames in method.__name__]

    segmentsize=3 # beta version not selectable from GUI at the moment
    bits=2 
    ordered_us_run(pcnames,bits,segmentsize,akt_odlist,def_odnames,segmentation)

def run_filteringxml(pointcloudpath,def_odnames,def_odlist,segmentsize,bits,segmentation,parameters):
    pcnames=pointcloudpath
    ordered_us_run(pcnames,bits,segmentsize, def_odlist,def_odnames,segmentation)
 

def abod_method(pc):
    od=create_abod_model(ABOD_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def cblof_method(pc):
    try:
        od=create_cblof_model(CBLOF_PARAMS)
        od.fit(pc[:,:3])
        detected=od.predict(pc[:,:3])
    except:
        detected= np.zeros((len(pc)))
    return detected

def copod_method(pc):
    od=create_copod_model(COPOD_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def ecod_method(pc):
    od=create_ecod_model(ECOD_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def inne_method(pc):
    od=create_inne_model(INNE_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def knn_method(pc):
    od=create_knn_model(KNN_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def loda_method(pc):
    od=create_loda_model(LODA_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def lof_method(pc):
    od=create_lof_model(LOF_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def pca_method(pc):
    od=create_pca_model(PCA_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def rod_method(pc):
    od=create_rod_model(ROD_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

def sampling_method(pc):
    od=create_sampling_model(SAMPLING_PARAMS)
    od.fit(pc[:,:3])
    detected=od.predict(pc[:,:3])
    return detected

# Outlier method constructors
def create_abod_model(params):
    return ABOD(contamination=params['contamination'], n_neighbors=params['n_neighbors'])

def create_cblof_model(params):
    return CBLOF(alpha=params['alpha'], beta=params['beta'], contamination=params['contamination'])

def create_copod_model(params):
    return COPOD(contamination=params['contamination'])

def create_ecod_model(params):
    return ECOD(contamination=params['contamination'])

def create_inne_model(params):
    return INNE(contamination=params['contamination'])

def create_knn_model(params):
    return KNN(contamination=params['contamination'])

def create_loda_model(params):
    return LODA(contamination=params['contamination'])

def create_lof_model(params):
    return LOF(contamination=params['contamination'])

def create_pca_model(params):
    return PCA(contamination=params['contamination'])

def create_rod_model(params):
    return ROD(contamination=params['contamination'])

def create_sampling_model(params):
    return FeatureBagging(base_estimator=None, contamination=params['contamination'])

# Outlier Method Parameters
ABOD_PARAMS = {'contamination': 0.1, 'n_neighbors': 10,}

CBLOF_PARAMS = {'alpha': 0.9, 'beta': 10, 'contamination': 0.1,}

COPOD_PARAMS = {'contamination': 0.1,}

ECOD_PARAMS = {'contamination': 0.1,}

INNE_PARAMS = {'contamination': 0.1,}

KNN_PARAMS = {'contamination': 0.1,}

LODA_PARAMS = {'contamination': 0.1,}

LOF_PARAMS = {'contamination': 0.1,}

PCA_PARAMS = {'contamination': 0.1,}

ROD_PARAMS = {'contamination': 0.1,}

SAMPLING_PARAMS = {'contamination': 0.1,}


def open_pointcloud(filenev):
    pc=np.loadtxt(filenev,delimiter=' ')
    return pc

def extents(ppoints):
    minx = np.amin(ppoints[:, 0])
    maxx = np.amax(ppoints[:, 0])
    miny = np.amin(ppoints[:, 1])
    maxy = np.amax(ppoints[:, 1])
    minz = np.amin(ppoints[:, 2])
    maxz = np.amax(ppoints[:, 2])
    return minx,maxx,miny,maxy,minz,maxz
def d1halfing_fast(pmin, pmax, pdepht):
    return np.linspace(pmin, pmax, 2 ** int(pdepht) + 1)

def octreecodes(ppoints, pdepht):
    minx, maxx, miny, maxy, minz, maxz=extents(ppoints)
    xletra = d1halfing_fast(minx, maxx, pdepht)
    yletra = d1halfing_fast(miny, maxy, pdepht)
    zletra = d1halfing_fast(minz, maxz, pdepht)
    otcodex = np.searchsorted(xletra, ppoints[:, 0], side='right') - 1
    otcodey = np.searchsorted(yletra, ppoints[:, 1], side='right') - 1
    otcodez = np.searchsorted(zletra, ppoints[:, 2], side='right') - 1
    out = otcodex * (2 ** (pdepht * 2)) + otcodey * (2 ** pdepht) + otcodez
    return (out, minx, maxx, miny, maxy, minz, maxz)

def usm_stepping(pc,step,start,order,sc=None):
    if order=='segment':
        out=pc[sc==start]
    else:   
        out=pc[start::step,:]
    return out

def pcorder(pc,occodes): #gives back the array in order by codes
    o=np.argsort(occodes)
    out=pc[o]
    return out

def segmentcodes(pc,segmentnum): #segmentnum*segmentum*segmentnum calculates all of the points of the cubes segmentnum>1 and <10
    minx, maxx, miny, maxy, minz, maxz=extents(pc)
    xosztas=np.linspace(minx, maxx, segmentnum)
    yosztas=np.linspace(miny, maxy, segmentnum)
    zosztas=np.linspace(minz, maxz, segmentnum)
    def scode(pont): #egy pont
        xcode=np.searchsorted(xosztas, pont[0], side='right') - 1
        ycode=np.searchsorted(yosztas, pont[1], side='right') - 1
        zcode=np.searchsorted(zosztas, pont[2], side='right') - 1
        return xcode+ycode*10+zcode*100
    pcsegcodes=[scode(x) for x in pc]
    return pcsegcodes

def getsegment(pc,segment,segmentpoz):
    return pc[pc[segmentpoz]==segment]

def getoverlapsegment(pc,segmentx,segmenty,segmentz, snum): #snum*snum*snum returns of the point of the given segement divided
    minx, maxx, miny, maxy, minz, maxz = extents(pc)
    xinter=(maxx-minx)/snum
    xgap=(maxx-minx)/(snum*20)
    xlow=minx+segmentx*xinter-xgap
    xhigh=minx+(segmentx+1)*xinter+xgap
    xfcontain=lambda x:x>=xlow and x<=xhigh
    yinter=(maxy-miny)/snum
    ygap=(maxy-miny)/(snum*20)
    ylow=miny+segmenty*yinter-ygap
    yhigh=miny+(segmenty+1)*yinter+ygap
    yfcontain=lambda y:y>=ylow and y<=yhigh
    zinter=(maxz-minz)/snum
    zgap=(maxz-minz)/(snum*20)
    zlow=minz+segmentz*zinter-zgap
    zhigh=minz+(segmentz+1)*zinter+zgap
    zfcontain=lambda z:z>=zlow and z<=zhigh
    xyzfun=lambda p:xfcontain(p[0]) and yfcontain(p[1]) and zfcontain(p[2])
    out=pc[xyzfun(pc)]
    return out

def filteredsave(filename,pc,method,subsampling,localfar):
    for i in range(len(method)):
        foutname=(method[i]+'_'+subsampling+'_'+localfar+'.xyz')
        np.savetxt(foutname, pc,fmt='%1.7f')
    return

def ordered_us_run(pcnames,bits,segmentsize,def_odlist,def_odnames, order=''):
    results=[]
    for pcname in pcnames:
        pc=open_pointcloud(pcnames)
        bit=bits
        codevalues=list(range(bit))
        if order=='octree':
            oc=octreecodes(pc,8)[0]
            pc=pcorder(pc, oc)
        if order=='segment':
            sc=segmentcodes(pc,segmentsize)
            bit=segmentsize**3
            codevalues=np.unique(sc)

        pc_test=pc[:,:3]

        if order=='segment':
            sc=segmentcodes(pc_test,segmentsize)
        else:
            sc=None

        start=time.time()
        emptyobject=[[] for x in range(pc.shape[1]+1)]
        for i in range(len(def_odlist)):
            odfun=def_odlist[i]
            start=time.time()
            elmentpc=np.array(emptyobject).T

            for step in codevalues: # if the segment method was called it it divides the point cloud into 40 bits
                aktpc=usm_stepping(pc_test, bit,step,order,sc)
                if len(aktpc)>10:
                    try:
                        labels=odfun(aktpc)
                    except Exception as e:
                        print('Did not work:',def_odnames,str(e))
                else:
                    labels=np.zeros((len(aktpc)))
                oneportion=np.c_[aktpc,labels] 
                elmentpc=np.concatenate((elmentpc, oneportion),axis=0)
            finishtime=time.time()-start
            results.append([pcname,def_odnames,finishtime]) 
            filteredsave(pcname, elmentpc, def_odnames, order,'local')
